import { Button } from '@progress/kendo-react-buttons';
import { Input } from '@progress/kendo-react-inputs';
import * as React from 'react';

import { IProject } from '../sidebar';
import './style.css';

export interface IFormProps {
  categoryId: number;
  onDelete: (categoryId: number, project: IProject) => void;
  onSave: (categoryId: number, project: IProject) => void;
  project: IProject;
}

interface IFormState {
  categoryId: number;
  editMode: boolean;
  project: IProject;
}

export class Form extends React.PureComponent<IFormProps, IFormState> {
  constructor(props: IFormProps) {
    super(props);

    this.state = {
      categoryId: 0,
      editMode: false,
      project: Object.assign({}, props.project)
    };

    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onNew = this.onNew.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  public componentWillReceiveProps(newProps: IFormProps):void {
    this.setState({
      categoryId: newProps.categoryId,
      editMode: false,
      project: Object.assign({}, newProps.project)
    });
  }

  public render() {
    return (
      <div className="form-wrapper">
        <div className="form-toolbar">
          {this.state.editMode &&
            <React.Fragment>
              <Button icon="save" title="Save" onClick={this.onSave} />
              <Button icon="cancel" title="Cancel" />
            </React.Fragment>
          }
          {!this.state.editMode &&
            <React.Fragment>
              <Button icon="plus" title="Create" onClick={this.onNew} />
              {this.state.project.id > 0 && <Button icon="edit" title="Edit" onClick={this.onEdit} />}
            </React.Fragment>
          }
          {this.state.project.id > 0 &&
            <Button icon="delete" title="Delete" onClick={this.onDelete} />
          }
        </div>
        <form className="form-body">
          <Input
            className="form-title"
            disabled={!this.state.editMode}
            onChange={this.onChangeTitle}
            value={this.state.project.title}
          />
          <textarea
            className="k-textarea form-description"
            disabled={!this.state.editMode}
            onChange={this.onChangeDescription}
            value={this.state.project.description}
            />
        </form>
      </div>
    );
  }

  private onChangeDescription(evt: any):void {
    this.setState({
      project: Object.assign({}, this.state.project, {
        description: evt.target.value
      })
    });
  }

  private onChangeTitle(evt: any):void {
    this.setState({
      project: Object.assign({}, this.state.project, {
        title: evt.target.value
      })
    });
  }

  private onDelete() {
    this.setState({
      editMode: false
    }, () => {
      this.props.onDelete(this.state.categoryId, this.state.project);
    });
  }

  private onEdit() {
    this.setState({
      editMode: true
    });
  }

  private onNew() {
    this.setState({
      editMode: true,
      project: {
        description: '',
        id: 0,
        title: ''
      }
    });
  }

  private onSave() {
    this.setState({
      editMode: false
    }, () => {
      this.props.onSave(this.state.categoryId, this.state.project);
    });
  }
}
