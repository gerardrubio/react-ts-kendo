import { Button } from '@progress/kendo-react-buttons';
import { Input } from '@progress/kendo-react-inputs';
import { PanelBar, PanelBarItem, PanelBarSelectEventArguments } from '@progress/kendo-react-layout';
import * as React from 'react';

import './style.css';

export interface IProject {
  description: string;
  id: number;
  title: string;
}

export interface ICategory {
  id: number;
  projects: IProject[];
  title: string;
}

interface ISidebarProps {
  categories: ICategory[];
  onCategorySelection: (id: number) => void;
  onProjectSelection: (id: number) => void;
}

interface ISidebarState {
  filter: string;
}

export class Sidebar extends React.PureComponent<ISidebarProps, ISidebarState> {
  constructor(props: ISidebarProps) {
    super(props);

    this.state = {
      filter: ''
    };

    this.onFilterChange = this.onFilterChange.bind(this);
    this.onSelectItem = this.onSelectItem.bind(this);
  }

  public onFilterChange(evt: React.ChangeEvent<HTMLInputElement>):void {
    this.setState({
      filter: evt.target.value || ''
    });
  }

  public render() {
    const categories = this.props.categories.map((category: ICategory) => {
      if (this.state.filter === '') {
        return category;
      }

      const categoryProjects = category.projects.filter((project: IProject) => project.title.toLocaleUpperCase().indexOf(this.state.filter.toLocaleUpperCase()) >= 0);

      if (categoryProjects.length > 0) {
        return {
          id: category.id,
          projects: categoryProjects,
          title: category.title,
        };
      }

      return null;
    })
    .filter((category: ICategory) => category !== null);

    return (
      <div className="sidebar-wrapper">
        <div className="sidebar-controls">
          <Input
            className="sidebar-filter"
            onChange={this.onFilterChange}
            value={this.state.filter}
          />
          <Button icon="plus" />
        </div>
        {categories.length > 0 &&
          <PanelBar
            expandMode="single"
            onSelect={this.onSelectItem}>
            {categories.map((category: ICategory) =>
              <PanelBarItem
                key={category.id}
                title={`${category.title} (${category.projects.length})`}
              >
                {category.projects.map((project: IProject) =>
                  <PanelBarItem key={project.id} title={project.title} />
                )}
              </PanelBarItem>
            )}
          </PanelBar>
        }
        {categories.length === 0 &&
          <div className="sidebar-empty">
            <em>No data available</em>
          </div>
        }
      </div>
    )
  }

  public onSelectItem(event: PanelBarSelectEventArguments):void {
    const key = event!.target!.key!.toString() || '';
    const segmentKey = key.split('$');
    if (segmentKey.length === 3) {
      this.props.onCategorySelection(Number(segmentKey[2]));
    } else if (segmentKey.length === 2) {
      this.props.onProjectSelection(Number(segmentKey[1]));
    }
  }
}
