import * as faker from 'faker';
import * as React from 'react';
import { ICategory, IProject, Sidebar } from './sidebar';

import '@progress/kendo-theme-default/dist/all.css';
import './App.css';
import { Form } from './form';

const sourceCategories: ICategory[] = [];
for (let i = 0; i < 10; i++) {
  const projects: IProject[] = [];
  for (let j = 0; j < faker.random.number(10); j++) {
    projects.push({
      description: faker.lorem.sentence(undefined, 4),
      id: j + 1,
      title: faker.commerce.department()
    })
  }
  sourceCategories.push({
    id: i + 1,
    projects,
    title: faker.commerce.department()
  });
}

interface IAppState {
  categories: ICategory[];
  category: number;
  project: IProject;
}

class App extends React.Component<{}, IAppState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      categories: Object.assign([], sourceCategories),
      category: 0,
      project: {
        description: '',
        id: 0,
        title: ''
      }
    };

    this.onDelete = this.onDelete.bind(this);
    this.onCategorySelection = this.onCategorySelection.bind(this);
    this.onProjectSelection = this.onProjectSelection.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  public render() {
    return (
      <div className="App">
        <Sidebar
          categories={this.state.categories}
          onCategorySelection={this.onCategorySelection}
          onProjectSelection={this.onProjectSelection}
        />
        <Form
          categoryId={this.state.category}
          onDelete={this.onDelete}
          onSave={this.onSave}
          project={this.state.project}
        />
      </div>
    );
  }

  private onCategorySelection(categoryId: number):void {
    this.setState({
      category: categoryId
    });
  }

  private onProjectSelection(projectId: number):void {
    const category = this.state.categories.find(cat => cat.id === this.state.category);
    let project:IProject;
    if (!!category) {
      project = category.projects.find(proj => proj.id === projectId) || {
        description: '',
        id: 0,
        title: ''
      };
    } else {
      project = {
        description: '',
        id: 0,
        title: ''
      };
    }
    this.setState({
      project
    })
  }

  private onSave(categoryId: number, project: IProject):void {
    const categories: ICategory[] = Object.assign([], this.state.categories);
    const category = categories.find(cat => cat.id === categoryId);
    if (!!category) {
      const existingProject = category.projects.find(proj => proj.id === project.id);
      if (!!existingProject) {
        existingProject.description = project.description;
        existingProject.title = project.title;
        this.setState({
          categories,
          project: existingProject
        });
      } else {
        let maxProjectId = 0;
        category.projects.forEach(proj => maxProjectId = Math.max(maxProjectId, proj.id));
        const newProject = Object.assign({}, project, { id: maxProjectId + 1 });
        category.projects.push(newProject);
        this.setState({
          categories,
          project: newProject
        });
      }
    }
  }

  private onDelete(categoryId: number, project: IProject):void {
    const categories: ICategory[] = Object.assign([], this.state.categories);
    const category = categories.find(cat => cat.id === categoryId);
    if (!!category) {
      const index = category.projects.findIndex(proj => proj.id === project.id);
      if (index >= 0) {
        category.projects.splice(index, 1);
        this.setState({
          categories
        });
      }
    }
  }
}

export default App;
